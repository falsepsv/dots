-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'activities'
-- Таблица активностей
-- ---

DROP TABLE IF EXISTS `activities`;
		
CREATE TABLE `activities` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `activity_type_id` INTEGER NULL DEFAULT NULL,
  `entity_id` INTEGER NULL DEFAULT NULL,
  `status_id` INTEGER NULL DEFAULT NULL,
  `create_date_time` DATETIME NULL DEFAULT NULL,
  `update_date_time` DATETIME NULL DEFAULT NULL,
  `city_id` INTEGER NULL DEFAULT NULL,
  `creator_id` INTEGER NULL DEFAULT NULL,
  `staff_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Таблица активностей';

-- ---
-- Table 'cities'
-- Таблица с городами
-- ---

DROP TABLE IF EXISTS `cities`;
		
CREATE TABLE `cities` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Таблица с городами';

-- ---
-- Table 'activity_types'
-- Типы активностей
-- ---

DROP TABLE IF EXISTS `activity_types`;
		
CREATE TABLE `activity_types` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Типы активностей';

-- ---
-- Table 'statuses'
-- Статусы
-- ---

DROP TABLE IF EXISTS `statuses`;
		
CREATE TABLE `statuses` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Статусы';

-- ---
-- Table 'tasks'
-- Задачи
-- ---

DROP TABLE IF EXISTS `tasks`;
		
CREATE TABLE `tasks` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `descr` MEDIUMTEXT NULL DEFAULT NULL,
  `lat` DOUBLE NULL DEFAULT NULL,
  `lon` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Задачи';

-- ---
-- Table 'projects'
-- Проекты
-- ---

DROP TABLE IF EXISTS `projects`;
		
CREATE TABLE `projects` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `descr` MEDIUMTEXT NULL DEFAULT NULL,
  `price` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Проекты';

-- ---
-- Table 'events'
-- События
-- ---

DROP TABLE IF EXISTS `events`;
		
CREATE TABLE `events` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `descr` MEDIUMTEXT NULL DEFAULT NULL,
  `required` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'События';

-- ---
-- Table 'activity_participants'
-- Участники активностей
-- ---

DROP TABLE IF EXISTS `activity_participants`;
		
CREATE TABLE `activity_participants` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `activity_id` INTEGER NULL DEFAULT NULL,
  `role_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Участники активностей';

-- ---
-- Table 'role_participants'
-- Роли участников
-- ---

DROP TABLE IF EXISTS `role_participants`;
		
CREATE TABLE `role_participants` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Роли участников';

-- ---
-- Table 'posts'
-- Посты
-- ---

DROP TABLE IF EXISTS `posts`;
		
CREATE TABLE `posts` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `activity_id` INTEGER NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `descr` MEDIUMTEXT NULL DEFAULT NULL,
  `create_date_time` DATETIME NULL DEFAULT NULL,
  `update_date_time` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Посты';

-- ---
-- Table 'images'
-- Картинки
-- ---

DROP TABLE IF EXISTS `images`;
		
CREATE TABLE `images` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `image_type_id` INTEGER NULL DEFAULT NULL,
  `entity_id` INTEGER NULL DEFAULT NULL,
  `deleted` TINYINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Картинки';

-- ---
-- Table 'image_types'
-- Типы картинок
-- ---

DROP TABLE IF EXISTS `image_types`;
		
CREATE TABLE `image_types` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Типы картинок';

-- ---
-- Table 'likes'
-- Лайки
-- ---

DROP TABLE IF EXISTS `likes`;
		
CREATE TABLE `likes` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `like_type_id` INTEGER NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `entity_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Лайки';

-- ---
-- Table 'like_types'
-- Тип сущности объекта лайка
-- ---

DROP TABLE IF EXISTS `like_types`;
		
CREATE TABLE `like_types` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Тип сущности объекта лайка';

-- ---
-- Table 'users'
-- Пользователи
-- ---

DROP TABLE IF EXISTS `users`;
		
CREATE TABLE `users` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `surname` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `lastname` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `pass_hash` VARCHAR(255) NULL DEFAULT NULL,
  `role_id` INTEGER NULL DEFAULT NULL,
  `city_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Пользователи';

-- ---
-- Table 'role_types'
-- Типы ролей
-- ---

DROP TABLE IF EXISTS `role_types`;
		
CREATE TABLE `role_types` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Типы ролей';

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `activities` ADD FOREIGN KEY (activity_type_id) REFERENCES `activity_types` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (entity_id) REFERENCES `tasks` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (entity_id) REFERENCES `projects` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (entity_id) REFERENCES `events` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (status_id) REFERENCES `statuses` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (city_id) REFERENCES `cities` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (creator_id) REFERENCES `users` (`id`);
ALTER TABLE `activities` ADD FOREIGN KEY (staff_id) REFERENCES `users` (`id`);
ALTER TABLE `activity_participants` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `activity_participants` ADD FOREIGN KEY (activity_id) REFERENCES `activities` (`id`);
ALTER TABLE `activity_participants` ADD FOREIGN KEY (role_id) REFERENCES `role_participants` (`id`);
ALTER TABLE `posts` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `posts` ADD FOREIGN KEY (activity_id) REFERENCES `activities` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (image_type_id) REFERENCES `image_types` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (entity_id) REFERENCES `activities` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (entity_id) REFERENCES `posts` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (entity_id) REFERENCES `users` (`id`);
ALTER TABLE `likes` ADD FOREIGN KEY (like_type_id) REFERENCES `like_types` (`id`);
ALTER TABLE `likes` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `likes` ADD FOREIGN KEY (entity_id) REFERENCES `posts` (`id`);
ALTER TABLE `likes` ADD FOREIGN KEY (entity_id) REFERENCES `activities` (`id`);
ALTER TABLE `users` ADD FOREIGN KEY (role_id) REFERENCES `role_types` (`id`);
ALTER TABLE `users` ADD FOREIGN KEY (city_id) REFERENCES `cities` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `activities` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cities` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `activity_types` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `statuses` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tasks` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `projects` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `events` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `activity_participants` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `role_participants` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `posts` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `images` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `image_types` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `likes` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `like_types` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `users` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `role_types` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `activities` (`id`,`activity_type_id`,`entity_id`,`status_id`,`create_date_time`,`update_date_time`,`city_id`,`creator_id`,`staff_id`) VALUES
-- ('','','','','','','','','');
-- INSERT INTO `cities` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `activity_types` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `statuses` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `tasks` (`id`,`title`,`descr`,`lat`,`lon`) VALUES
-- ('','','','','');
-- INSERT INTO `projects` (`id`,`title`,`descr`,`price`) VALUES
-- ('','','','');
-- INSERT INTO `events` (`id`,`title`,`descr`,`required`) VALUES
-- ('','','','');
-- INSERT INTO `activity_participants` (`id`,`user_id`,`activity_id`,`role_id`) VALUES
-- ('','','','');
-- INSERT INTO `role_participants` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `posts` (`id`,`user_id`,`activity_id`,`title`,`descr`,`create_date_time`,`update_date_time`) VALUES
-- ('','','','','','','');
-- INSERT INTO `images` (`id`,`url`,`image_type_id`,`entity_id`,`deleted`) VALUES
-- ('','','','','');
-- INSERT INTO `image_types` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `likes` (`id`,`like_type_id`,`user_id`,`entity_id`) VALUES
-- ('','','','');
-- INSERT INTO `like_types` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `users` (`id`,`surname`,`name`,`lastname`,`email`,`pass_hash`,`role_id`,`city_id`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `role_types` (`id`,`name`) VALUES
-- ('','');