module.exports = {
  apps : [{
    name: 'API',
    script: "bin/www",
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    // args: '',
    instances: 5,
    autorestart: true,
    watch: false,
    max_memory_restart: '512M',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production',
      PORT: '5555'
    }
  }]
};