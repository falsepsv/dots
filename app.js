var config = require('./config/config');

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var busRouter = require('./routes/bus');

const bodyParser = require('body-parser')
const cons = require('consolidate');

var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth2').Strategy;

var app = express();

passport.serializeUser(function (user, done) {
  console.log('serialize user');
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  console.log('deserialize user');
  done(null, obj);
});

passport.use(new GoogleStrategy(config.googleAuth,
  function (request, accessToken, refreshToken, profile, done) {
    console.log(accessToken, refreshToken, profile, done);
    // TODO Найти или создать юзера в бд
    return done(null, profile);
  }
));

app.use(passport.initialize());
app.use(passport.session());

//Смотрим какой у нас путь до статике(соединение текущего положение с папкой клиент)
const pathClient = path.join(__dirname,'client')
//Используем парсер, чтобы принимать post запросы
app.use(bodyParser.json())

// view engine setup
app.engine('html', cons.swig)
app.set('views', pathClient);
app.set('view engine', 'html');
//Указываем серверу папку со статикой
app.use(express.static(pathClient))
// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'html');
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser('keyboard cat'));
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 60000
  }
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bus', busRouter);


app.get('/auth/google',
  passport.authenticate('google', {
    scope: ['email', 'profile']
  }));

app.get('/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: '/',
    failureRedirect: '/'
  }));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;