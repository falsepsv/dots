var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
  // res.render('index2', {
  //   title: 'Express',
  //   user: req.session && req.session.passport && req.session.passport.user && req.session.passport.user.displayName || 'anonymous' // я потом подумаю как сделать норм :)
  // });
});
router.get('/statistic', function (req, res, next) {
  res.render('statistic');
  
});
router.get('/detection', function (req, res, next) {
  res.render('detection');
  
});

module.exports = router;