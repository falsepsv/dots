var express = require('express');
var router = express.Router();
var random = (min, max) => Math.round(min - 0.5 + Math.random() * (max - min + 1));
const knex = require('knex')({
    client: 'pg',
    connection: {
        host: 'maxopka.band',
        port: 5432,
        user: 'dots',
        password: 'fsaDSf5r',
        database: 'dots'
    },
    useNullAsDefault: true
});

/**
 * Маршруты
 * http://localhost:3000/bus/routes
 */
router.get('/routes', function (req, res, next) {
    knex('public.routes')
        .then(result => res.send(result))
        .catch(error => res.send(error));
});

/**
 * Остановки для маршрута по порядку
 * http://localhost:3000/bus/checkpoints?route=33
 * http://localhost:3000/bus/checkpoints?route=28
 */
router.get('/checkpoints', function (req, res, next) {
    knex('public.route_checkpoints')
        .where({
            route_id: req.param('route')
        })
        .then(result => res.send(result))
        .catch(error => res.send(error));
});

/**
 * Ближайший автобус к остановке
 * Рандомит 
 * начения исторические и текущие
 * currentState.passedCheckpoints и historicalState.checkpoints содержат массив, в котором
 * delta - сколько добаввлось или убавилось пассажиров
 * passengersCount - сколько есть пассажиров
 * name - назваине остановки
 * @param route {Number} id маршрута (у 30 маршрута id = 33, у 25-го маршрута id = 28)
 * http://localhost:3000/bus/closer?route=33&checkpoint=Цветной%20бульвар%201&isforward=1
 */
router.get('/closer', async function (req, res, next) {
    const [checkpoint, time, route, isForward] = [req.param('checkpoint'),
        req.param('time'),
        req.param('route'),
        !!parseInt(req.param('isforward'))
    ];

    let checkpoints = await knex('public.route_checkpoints')
        .where({
            route_id: route
        });

    const currentCheckpointIndex = checkpoints.findIndex(check => check.name === checkpoint && check.is_forward === isForward),
        passedCheckpoints = checkpoints.slice(0, currentCheckpointIndex + 1);

    res.send({
        currentState: { // текущее состояние
            passedCheckpoints: passedCheckpoints.reduce((acc, checkpoint) => {
                const prev = acc.length > 0 ? acc[acc.length - 1] : {
                    passengersCount: 0
                };

                const delta = random(-15, 15); // изменение кол-ва пассажиров
                acc.push({
                    name: checkpoint.name,
                    passengersCount: prev.passengersCount + delta >= 0 ? prev.passengersCount + delta : 0,
                    delta: delta
                });

                return acc;
            }, [])
        },
        historicalState: {
            checkpoints: checkpoints.reduce((acc, checkpoint) => {
                const prev = acc.length > 0 ? acc[acc.length - 1] : {
                    passengersCount: 0
                };

                const delta = random(-15, 15); // изменение кол-ва пассажиров
                acc.push({
                    name: checkpoint.name,
                    passengersCount: prev.passengersCount + delta >= 0 ? prev.passengersCount + delta : 0,
                    delta: delta
                });

                return acc;
            }, [])
        }

    });
    console.log('');
});

// http://localhost:3000/bus/stats?route=33
router.get('/stats', async function (req, res, next) {
    try {
        const route = req.param('route');

        const checkpoints = await knex('public.route_checkpoints')
            .where({
                route_id: route
            });


        const hours = Array.apply(null, {
                length: 24
            }).map(Number.call, Number)
            .map(number => `${number}:00`);

            console.warn(checkpoints);
        let result = checkpoints.map(point => {
            const hoursValues = hours.map((hour, index) => {
                let coef = 1,
                    min = 5;

                if (index > 6 && index < 10 || index > 16 && index < 20) {
                    min = 15;
                    coef = 2;
                }

                return {
                    name: point.name,
                    isForward: point.is_forward,
                    hour: hour,
                    passengers: random(min, 35 * coef)
                }

            });

            return hoursValues;
        });

        result = result.reduce((acc, record) => {
            acc.push(...record);
            return acc
        }, [])

        res.send(result);
    } catch (error) {
        console.log(error);
        res.send(error.message);
    }
});



module.exports = router;