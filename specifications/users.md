## Пользователи

##### Получить список юзеров
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/users/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : [
        {
            id: 1,
            surname: "Иванович",
            name: "Иван",
            lastname: "Иванов",
            email: "test@mail.ru",  
            city:{
                id: 1,
                name: "Тюмень"
            },
            images:[
                {
                    id: 23,
                    url:"asd"
                }
            ]
        }
    ]
}
```

##### Регистрация
Запрос
```$xslt
curl -i -g -X POST -d 
`{
    surname: "Иванович",
    name: "Иван",
    lastname: "Иванов",
    email: "test@mail.ru",  
    city: 2,
    password: "test"
    images:[
        {            
            url:"asd"
        }
    ]
}` 'http://добрыедела.рус/api/v1/users/register'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```

##### Вход
Запрос
```$xslt
curl -i -g -X POST -d 
`{    
    email: "test@mail.ru",    
    password: "test"
}` 'http://добрыедела.рус/api/v1/users/login'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        token: "asfdsdaf7f87asfvasv"
    }
}
```

##### Профиль
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/users/profile'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        surname: "Иванович",
        name: "Иван",
        lastname: "Иванов",
        email: "test@mail.ru",  
        city: {
            id: 3,
            name: "Тюмень"
        },
        role: {
            id: 3,
            name: "admin"
        },        
        images:[
            {       
                id:4, 
                url:"asd"
            }
        ]
     }
}
```