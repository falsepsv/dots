## Активности

##### Получить список активностей
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/activities/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : [
        {
            id: 1,
            activity_type:{id:1, name:"tasks"},
            entity:{
                id:1, title: "Мусор", descr: "На пляже лежит мусор", 
                lat: 57.346534, lon: 65.345634
            },
            status:{
                id: 2,
                name: "progress"
            },
            create_date_time: "d-m-Y H:i:s",
            update_date_time: "d-m-Y H:i:s",
            city:{
                id: 2,
                name: "Тюмень"
            },
            creator_id: 12,
            staff_id: 34,
            images: [
                {
                    id: 1,
                    url:"http//cdn/test.jpg"                    
                }
            ]
        }
    ]
}
```

##### Создать активность
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X POST -d 
`{
    activities:[
        {
            activity_type_id: 2,
            entity_id: 56,
            status_id: 3,
            create_date_time: "d-m-Y H:i:s",
            update_date_time: "d-m-Y H:i:s",
            city_id: 4,
            entity:{
                title: "Благотворительность",
                descr: "Хочу собрать вещи детям",
                price: 5000,
                creator: 3
            },
            images: [
                {                    
                    url:"http//cdn/test.jpg"                    
                }
            ]
        }
    ]
}` 'http://добрыедела.рус/api/v1/activities/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : [56]
}
```

##### Обновить активность
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X PATCH -d 
`{
    activities:[
        {       
            status_id: 6,
            update_date_time: "d-m-Y H:i:s",
            entity:{
                staff_id: 34
            },
            images: [
                {
                    id: 1,
                    deleted: true                    
                }
            ]
        }
    ]
}` 'http://добрыедела.рус/api/v1/activities/12'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```

##### Удалить активность
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X DELETE 'http://добрыедела.рус/api/v1/activities/12'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```