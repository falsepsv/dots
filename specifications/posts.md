## Посты

##### Получить список постов
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/posts/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : [
        {
            id: 1,
            user_id:{id:1, name:"Vova", surname: "Ivanov"},
            activity_id: 3,            
            create_date_time: "d-m-Y H:i:s",
            update_date_time: "d-m-Y H:i:s",
            title: "футбол",
            descr: "Классно провёл время...",
            images: [
                {
                    id: 1,
                    url:'http://cdn/test.jpg'                    
                }
            ]
        }
    ]
}
```

##### Создать пост
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X POST -d 
`{
    posts:[
        {
            activity_id: 2,
            create_date_time: "d-m-Y H:i:s",
            update_date_time: "d-m-Y H:i:s",
            title: "футбол",
            descr: "Классно провёл время...",
            images: [
                {                    
                    url:'http://cdn/test.jpg'                    
                }
            ]          
        }
    ]
}` 'http://добрыедела.рус/api/v1/posts/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : [325]
}
```

##### Обновить пост
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X PATCH -d 
`{
    posts:[
        {       
            activity_id: 2,
            update_date_time: "d-m-Y H:i:s",
            title: "футбол2",
            descr: "Классно2 провёл время...",
            images: [
                {
                    id: 1,
                    deleted:true                    
                }
            ]
        }
    ]
}` 'http://добрыедела.рус/api/v1/posts/12'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```

##### Удалить пост
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X DELETE 'http://добрыедела.рус/api/v1/posts/12'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```