## Лайки

##### Получить кол-во лайков по одной сущности
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/likes/count/${like_type}/${entity_id}'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        count: 34
    }
}
```

##### Получить кол-во лайков по всем сущностям у одного человека
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X GET 'http://добрыедела.рус/api/v1/likes/totalCount/${user_id}'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        count: 3245
    }
}
```

##### Лайк\Дизлайк
Запрос
```$xslt
curl -i -g -H "Autorization:Bearer fgs876fdb786df87g68df7" -X POST -d 
`{
    like_type_id: 1,
    entity_id: 23,
    like: true
}` 'http://добрыедела.рус/api/v1/likes/'
```

Ответ
```$xslt
{
    "success": true,
    "status": 200,
    "data" : {
        result: true
    }
}
```
