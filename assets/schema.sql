DROP TABLE IF EXISTS private.terminal_records;
DROP TABLE IF EXISTS private.buses;
DROP TABLE IF EXISTS private.routes_checkpoint_order;
DROP TABLE IF EXISTS private.schedule;
DROP TABLE IF EXISTS private.routes;
DROP TABLE IF EXISTS private.checkpoints;

-- TRUNCATE TABLE private.terminal_records CASCADE;
-- TRUNCATE TABLE private.buses CASCADE;
-- TRUNCATE TABLE private.routes_checkpoint_order CASCADE;
-- TRUNCATE TABLE private.schedule CASCADE;
-- TRUNCATE TABLE private.routes CASCADE;
-- TRUNCATE TABLE private.checkpoints CASCADE;

CREATE TABLE private.checkpoints
(
  id         serial PRIMARY KEY NOT NULL,
  is_forward BOOLEAN            NOT NULL,
  name       TEXT,
  UNIQUE (NAME, is_forward)
);

CREATE TABLE private.routes
(
  id   serial PRIMARY KEY NOT NULL,
  name TEXT
);

CREATE TABLE private.routes_checkpoint_order
(
  id            serial PRIMARY KEY NOT NULL,
  route_id      INT                NOT NULL REFERENCES private.routes (id),
  checkpoint_id INT                NOT NULL REFERENCES private.checkpoints (id),
  number        INT
);

CREATE TABLE private.schedule
(
  id            serial PRIMARY KEY NOT NULL,
  checkpoint_id INT                NOT NULL REFERENCES private.checkpoints (id),
  route_id      INT                NOT NULL REFERENCES private.routes (id),
  time          TIME
);

CREATE TABLE private.buses
(
  id           serial PRIMARY KEY NOT NULL,
  state_number TEXT               NOT NULL
);

CREATE TABLE private.terminal_records
(
  id         serial PRIMARY KEY NOT NULL,
  time       TIMESTAMP          NOT NULL,
  route_id   INT                NOT NULL REFERENCES private.routes,
  bus_id     INT                NOT NULL DEFAULT NULL,
  race_start TIMESTAMP          NOT NULL,
  race_end   TIMESTAMP          NOT NULL
);


INSERT INTO private.routes (name)
SELECT DISTINCT route
FROM private.__raw_payments
ORDER BY route;

SELECT *
FROM private.routes;

INSERT INTO private.buses (state_number)
SELECT DISTINCT bus_state_number
FROM private.__raw_payments
GROUP BY bus_state_number;

INSERT INTO private.terminal_records (time, route_id, bus_id, race_start, race_end)
SELECT time::TIMESTAMP                                                                        AS time,
       (SELECT id from private.routes WHERE raw.route = private.routes.name)                  AS route_id,
       (SELECT id from private.buses WHERE raw.bus_state_number = private.buses.state_number) AS bus_id,
       race_start::TIMESTAMP                                                                  AS race_start,
       race_end::TIMESTAMP                                                                    AS race_end
FROM private.__raw_payments raw
WHERE race_end IS NOT NULL;

-- TRUNCATE TABLE private.routes_checkpoint_order;
-- TRUNCATE TABLE private.schedule;
-- TRUNCATE TABLE private.checkpoints CASCADE;

DROP VIEW IF EXISTS public.route_checkpoints;
CREATE OR REPLACE VIEW public.route_checkpoints AS
SELECT route_id,
       routes.name as route,
       checks.name as checkpoint,
       checks.is_forward,
       number
FROM private.routes routes
       INNER JOIN private.routes_checkpoint_order orders ON routes.id = orders.route_id
       INNER JOIN private.checkpoints checks ON orders.checkpoint_id = checks.id
ORDER BY routes.id, orders.number;

DROP VIEW IF EXISTS public.routes;
CREATE OR REPLACE VIEW public.routes AS
SELECT DISTINCT route_id    AS id,
                routes.name AS name
FROM private.routes_checkpoint_order orders
       INNER JOIN private.routes routes ON orders.route_id = routes.id;


SELECT *
FROM private.terminal_records
WHERE route_id = 33
  AND time < '2019-05-03 10:31:00' AND TIME > '2019-05-03 10:00:00'
ORDER BY race_start