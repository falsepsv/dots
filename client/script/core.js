class Core {
    constructor() {
        this.state = {
            'selectTrip': 28
        };
        this.app = document.getElementById('app');
        
        // app.innerHTML = 'rarara';
        this.initListeners();

        this.options = new Options(this.app, this.state.selectTrip);

        // this.realData = this.getMokaData();
        // this.reallife = new Reallife(this.realData, this.app);

        // this.frame = new Frame( this.app);
        this.diagrams = new Diagrams( this.app);
    }
    initListeners() {
        BusEvent.on('changeTrip', (data) => {
            this.state.selectTrip = data;
            this.options.listDirections.innerHTML = this.options.getListDirections();
            // this.options.selectTrip.innerHTML = this.options.getListTrips();    
            console.log(data)
        });
        BusEvent.on('downloadData', (data) => {
            let {
                date1,
                date2,
                trip,
                direction
            } = data;
            fetch(`/bus/stats?route=${trip}`).then(response => {
                return response.json();
            }).then(data => {
                this.diagrams.setDiagram(data.filter(record => record.isForward === !!parseInt(direction)));
            });
        });
    }
    getMokaData() {
        return {
            'direction_1': [
                "Ж/д вокзал",
                "Сквер Семёна Пацко",
                "Смоленская",
                "Цветной бульвар",
                "Центральный рынок",
                "Завод Станкостроительный",
                "Холодильная",
                "пл. Памяти",
                "Магазин Буратино",
                "ДК Строитель",
                "Пермякова",
                "Тюменский м-н",
                "МЖК",
                "Театр Ангажемент",
                "Моторостроителей",
                "Поликлиника",
                "Восточный 2",
                "Восточный 3",
                "Поворот на ТЭЦ-2",
                "ТЭЦ-2",
                "Боровская",
                "Станция Войновка",
                "Таллинская"
            ],
            'direction_2': [
                "Таллинская",
                "Станция Войновка",
                "Боровская",
                "ТЭЦ-2",
                "Поворот на ТЭЦ-2",
                "Восточный 3",
                "Восточный 2",
                "Поликлиника",
                "Моторостроителей",
                "Театр Ангажемент",
                "Сквер Депутатов",
                "Тюменский м-н",
                "3-й м-н",
                "Торговый центр",
                "Пермякова",
                "ДК Строитель",
                "Театральный центр Космос",
                "Газпром",
                "Холодильная",
                "Максима Горького (Республики)",
                "пл. Центральная",
                "Первомайская",
                "Герцена (Первомайская)",
                "Ж/д вокзал"
            ]
        };

    }
}