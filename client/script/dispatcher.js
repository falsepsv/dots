class Dispatcher{
    constructor(){
        this.listeners = {};
    }
    on(eventName, func) {
        if (typeof this.listeners[eventName] === 'undefined') {
            this.listeners[eventName] = [];
        }

        this.listeners[eventName].push(func);
    }

    trigger(eventName, data) {
        if (typeof this.listeners[eventName] === 'undefined') return;

        this.listeners[eventName].forEach(function(handler) {
            handler(data);
        });
    }
}



