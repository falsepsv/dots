class Reallife{
    constructor(realData, parrent){
        this.realData = realData;
        this.parrent = parrent;
        let content = document.createElement('div');
        content.id = 'reallife';
        this.content = content;

        content.innerHTML = `
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Выберете остановку:</label>
                        <div id="listStopPoints" class="col-sm-8">
                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 10</div>
                            </div>

                            
                            <i class="material-icons bus">navigation</i>
                            

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 9</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                
                                <i class="material-icons bus">navigation</i>                                

                                <div class="stopPoint stopPoint--active"></div>
                                <div>Остановка 8</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 7</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 6</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 5</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 4</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 3</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 2</div>
                            </div>

                            <div class="stopBridge"></div>

                            <div class="rowStopPoint">
                                <div class="stopPoint"></div>
                                <div>Остановка 1</div>
                            </div>

                        </div>
                    </div>
        `;
        parrent.appendChild(content);


        // BusEvent.trigger('test', 'hihihi');
    }
//     <i class="material-icons">
// navigation
// </i>
// <i class="material-icons">
// trip_origin
// </i>
    
}