class Frame{
    constructor(parrent){
        this.parrent = parrent;

        this.status = {
            'empty':'text-secondary',
            'little':'text-warning',
            'many':'text-success',
            'full':'text-danger'
        };

        let content = document.createElement('div');
        content.id = 'frame';
        content.innerHTML = `

            <div class="row">
                <label class="col-sm-2 col-form-label">Статус:</label>
                <div class="form-control col-sm-4 text-danger">
                    Забит
                </div>
            </div>
            <div id="screenshot">                
                <img src="../images/bus.jpg"/>                
            </div>
        `;
        this.content = content;

        parrent.appendChild(content);
    }
}