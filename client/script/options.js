class Options {
    constructor(parrent, selectTrip) {
        this.parrent = parrent;
        let content = document.createElement('div');
        content.id = 'options';
        this.content = content;
        parrent.appendChild(content);
        Promise.all([
            fetch('/bus/routes').then(response => response.json()),
            fetch('/bus/routes').then(response => response.json()),
            fetch('/bus/routes').then(response => response.json())
        ]);

        const trips = [];
        fetch('/bus/routes').then(response => response.json())
            .then(routes => {
                return Promise.all(routes.map(async route => {
                    const response = await fetch('/bus/checkpoints?route=' + route.id);
                    const checkpoints = await response.json();
                    const forward = checkpoints.filter(p => p.is_forward);
                    const backward = checkpoints.filter(p => !p.is_forward);
                    const result = {
                        name: route.name,
                        value: route.id,
                        directions: [{
                            direction: 1,
                            name: forward[forward.length - 1].name
                        }, {
                            direction: 0,
                            name: backward[forward.length - 1].name
                        }, ]
                    };
                    trips.push(result);
                    return result;
                }));
            }).then(trip1s => {
                console.log(trip1s);
                this.state = {
                    selectTrip: selectTrip,
                    trips: trips
                };


                content.innerHTML = `
                <form id="form">
                <!--
                type="date" value="2011-08-19"     
                type="time" value="13:45:00"
                -->
                <div class="form-group row">
                    <label for="datetimeFrom" class="col-sm-4 col-form-label">Период времени от:</label>
                    <div class="col-8">
                        <input class="form-control" type="date" value="2019-08-19"  id="datetimeFrom">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="datetimeTo" class="col-sm-4 col-form-label">Период времени до:</label>
                    <div class="col-8">
                        <input class="form-control" type="date" value="2019-08-19"  id="datetimeTo">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="selectTrip" class="col-sm-4 col-form-label">Выберете маршрут:</label>
                    <div class="col-sm-8">
                        <select id="selectTrip" class="custom-select custom-select-lg mb-3">
                            ${this.getListTrips()}    
                        </select>                    
                    </div>
                </div>

                
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-4 pt-0">Направление:</legend>
                        <div class="col-sm-8" id="listDirections">
                            ${this.getListDirections()}   
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <div class="col-sm-4 offset-sm-4">
                        <button id="downloadData" type="submit" class="btn btn-primary">Загрузить данные</button>
                    </div>
                </div>

                <!--
                <div class="form-group row">
                    <div class="col-sm-2">Checkbox</div>
                    <div class="col-sm-10">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck1">
                        <label class="form-check-label" for="gridCheck1">
                            Example checkbox
                        </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                </div>-->
            </form>


            
            
        `;
                // parrent.appendChild(content);
                this.initListeners();
            });

        // this.state = {
        //     'selectTrip': selectTrip,
        //     'trips': [{
        //             'name': '25',
        //             'value': 28,
        //             'directions': [{
        //                     'direction': 1,
        //                     'name': 'ЖД вокзал'
        //                 },
        //                 {
        //                     'direction': 2,
        //                     'name': 'Войновка'
        //                 }
        //             ]

        //         },
        //         {
        //             'name': '30',
        //             'value': 33,
        //             'directions': [{
        //                     'direction': 1,
        //                     'name': 'Государственный Аграрный Университет'
        //                 },
        //                 {
        //                     'direction': 2,
        //                     'name': 'Восточный м-н'
        //                 }
        //             ]

        //         }
        //     ]
        // };

        // content.innerHTML = `
        //         <form id="form">
        //         <!--
        //         type="date" value="2011-08-19"     
        //         type="time" value="13:45:00"
        //         -->
        //         <div class="form-group row">
        //             <label for="datetimeFrom" class="col-sm-4 col-form-label">Период времени от:</label>
        //             <div class="col-8">
        //                 <input class="form-control" type="date" value="2019-08-19"  id="datetimeFrom">
        //             </div>
        //         </div>
        //         <div class="form-group row">
        //             <label for="datetimeTo" class="col-sm-4 col-form-label">Период времени до:</label>
        //             <div class="col-8">
        //                 <input class="form-control" type="date" value="2019-08-19"  id="datetimeTo">
        //             </div>
        //         </div>


        //         <div class="form-group row">
        //             <label for="selectTrip" class="col-sm-4 col-form-label">Выберете маршрут:</label>
        //             <div class="col-sm-8">
        //                 <select id="selectTrip" class="custom-select custom-select-lg mb-3">
        //                     ${this.getListTrips()}    
        //                 </select>                    
        //             </div>
        //         </div>


        //         <fieldset class="form-group">
        //             <div class="row">
        //                 <legend class="col-form-label col-sm-4 pt-0">Направление:</legend>
        //                 <div class="col-sm-8" id="listDirections">
        //                     ${this.getListDirections()}   
        //                 </div>
        //             </div>
        //         </fieldset>
        //         <div class="form-group row">
        //             <div class="col-sm-4 offset-sm-4">
        //                 <button id="downloadData" type="submit" class="btn btn-primary">Загрузить данные</button>
        //             </div>
        //         </div>

        //         <!--
        //         <div class="form-group row">
        //             <div class="col-sm-2">Checkbox</div>
        //             <div class="col-sm-10">
        //                 <div class="form-check">
        //                 <input class="form-check-input" type="checkbox" id="gridCheck1">
        //                 <label class="form-check-label" for="gridCheck1">
        //                     Example checkbox
        //                 </label>
        //                 </div>
        //             </div>
        //         </div>
        //         <div class="form-group row">
        //             <div class="col-sm-10">
        //                 <button type="submit" class="btn btn-primary">Sign in</button>
        //             </div>
        //         </div>-->
        //     </form>




        // `;
        // parrent.appendChild(content);
        // this.initListeners();
    }
    getListTrips() {
        let content = ``;

        for (let i = 0; i < this.state.trips.length; i++) {
            let opt = this.state.trips[i];
            let check = opt.value === this.state.selectTrip;
            content += `<option ${check?'selected':''}  value="${opt.value}">${opt.name}</option>`;
        }
        return content;
    }

    getListDirections() {
        let currentDirections = this.getCurrentDirections();

        if (currentDirections.length < 1) {
            return `
            <div class="form-check">
                
            </div>
            `;
        }

        let content = ``;

        for (let i = 0; i < currentDirections.length; i++) {
            let opt = currentDirections[i];
            // let check = opt.value===this.state.selectTrip;
            content += `
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gridRadios" id="direction_${opt.direction}" value="${opt.direction}" ${i===0?'checked':''}>
                    <label class="form-check-label" for="direction_${opt.direction}">
                    ${opt.name}
                    </label>
                </div>
            `;
        }
        return content;
    }
    getCurrentDirections() {
        let directions = [];

        for (let i = 0; i < this.state.trips.length; i++) {
            if (this.state.trips[i].value === this.state.selectTrip) {
                directions = this.state.trips[i].directions;
                break;
            }

        }

        return directions;
    }

    initListeners() {
        this.selectTrip = document.getElementById('selectTrip');
        this.selectTrip.addEventListener('click', (event) => {
            if (parseInt(event.target.value) === 0 || parseInt(event.target.value) === this.state.selectTrip)
                return;
            this.state.selectTrip = parseInt(event.target.value);
            BusEvent.trigger('changeTrip', event.target.value);
        });

        this.listDirections = document.getElementById('listDirections');
        this.datetimeFrom = document.getElementById('datetimeFrom');
        this.datetimeTo = document.getElementById('datetimeTo');
        this.form = document.getElementById('form');
        this.directions = document.getElementsByClassName('form-check-input');

        this.downloadData = document.getElementById('downloadData');
        this.downloadData.addEventListener('click', (event) => {
            event.preventDefault();
            let date1 = this.datetimeFrom.value;
            let date2 = this.datetimeTo.value;
            let trip = this.selectTrip.value;

            let direction = 1;
            for (let i = 0; i < this.directions.length; i++) {
                let dir = this.directions[i];
                if (dir.checked) {
                    direction = dir.value;
                    break;
                }
            }
            console.log(date1);
            console.log(date2);
            console.log(trip);
            console.log(direction);
            BusEvent.trigger('downloadData', {
                "date1": date1,
                "date2": date2,
                "trip": trip,
                "direction": direction
            });
        });
    }
}