class Detection{
    constructor(){        
        this.selectedFile = null;
        this.main = document.getElementById('detection');
        this.main.innerHTML = `
        <div class="file-upload">
            <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>
            <button id="file-detection-btn" class="file-detection-btn" type="button">Detection</button>
        
            <div class="image-upload-wrap">
            <input id="file-upload" class="file-upload-input" type='file' accept="image/*" />
            <div class="drag-text">
                <h3>Drag and drop a file or select add Image</h3>
            </div>
            </div>
            <div class="file-upload-content">
            <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
                <button id="remove-image" type="button" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
            </div>
        </div>
        `;
        
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });
        this.initListeners();
    }
    readURL(event){
        let input = event.target;
        if (input.files && input.files[0]) {
      
          var reader = new FileReader();
      
          reader.onload = function(e) {
            $('.image-upload-wrap').hide();
      
            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();
      
            $('.image-title').html(input.files[0].name);
          };
      
          reader.readAsDataURL(input.files[0]);
          this.selectedFile = input.files[0];
        } else {
          this.removeUpload();
        }
        this.initListeners();
    }
      
    removeUpload(){
        this.selectedFile = null;
        $('#file-upload').replaceWith($('#file-upload').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();

        this.initListeners();
    }
    initListeners(){
        document.getElementById('file-upload').onchange = (e)=>{
            this.readURL(e);
        };
        document.getElementById('remove-image').onclick = (e)=>{
            this.removeUpload(e);
        };

        document.getElementById('file-detection-btn').onclick = (e)=>{
            console.log(this.selectedFile);
            if(this.selectedFile===null)
                return;

            var fd = new FormData();    
            fd.append( 'file', this.selectedFile );
            $.post( 'http://localhost:3000/detection', fd, (res)=>{
                $('.file-upload-image').attr('src', res);
            } );
        };
    }
          
}